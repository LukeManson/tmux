# tmux Config

## Setup

tmux uses a `.tmux.conf` file in the user's home directory to load config. I have moved this config to `~/.config/tmux/tmux.conf`. The file that tmux loads by default should have just a stingle line telling it to source the abstracted file. This is mostly so I can make `~/.config/tmux/` into a git repo.

```sh
git clone git@gitlab.com:LukeManson/tmux.git ~/.config/tmux
echo "source-file ~/.config/tmux/tmux.conf" > ~/.tmux.conf
```
